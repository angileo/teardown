class ApplicationMailer < ActionMailer::Base
  default from: "angel.bookmart@gmail.com"
  layout 'mailer'
end
