class ListingImage < ActiveRecord::Base
  belongs_to :listing
  validates :url,
            presence: true

  def to_s
    url
  end
end
