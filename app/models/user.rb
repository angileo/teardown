class User < ActiveRecord::Base
  has_many :listings, dependent: :destroy
  attr_accessor :activation_token
  before_create :create_activation_digest
  before_save{self.email=email.downcase}
  validates :first_name, :last_name, presence:true
  VALID_EMAIL_REGEX=/\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email,presence:true,length:{maximum:255},
            format:{with:VALID_EMAIL_REGEX}, uniqueness:{case_sensitive:false}
  validates :password,length: {minimum:6}

  def User.new_token
    SecureRandom.urlsafe_base64
  end

  def User.digest(string)
    cost=ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
    BCrypt::Password.create(string,cost:cost)
  end

  def authenticate_my_user? string
    self.password == string
  end

  def authenticated?(attribute,token)
    digest=send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end
  private
  # Creates and assigns the activation token and digest.
  def create_activation_digest
  self.activation_token=User.new_token
  self.activation_digest=User.digest(activation_token)
end
end
