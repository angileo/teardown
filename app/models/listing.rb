class Listing < ActiveRecord::Base
  belongs_to :user
  has_many :listing_images, :dependent => :destroy
  has_many :line_items
  before_destroy :ensure_not_referenced_by_any_line_item
  accepts_nested_attributes_for :listing_images,
                                reject_if: proc { |attributes| attributes['url'].blank? }, allow_destroy: true
  validates :user_id, presence: true
  self.per_page = 10
  validates :street_address, presence:true

  private

  def ensure_not_referenced_by_any_line_item
    if line_items.empty?
      return true
    else
      errors.add(:base, 'line items present')
      return false
    end
  end
end
