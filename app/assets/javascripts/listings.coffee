# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on 'page:change', ->
  carouselCount = 0
  nextVar = 0
  prevVar = 0
  $('.carousel_show').jCarouselLite
    btnNext: '.next'
    btnPrev: '.prev'
    visible: 1
    auto: 800
    speed: 2000
  $('button[class=\'prev\']').each ->
    $(this).attr 'class', 'prev' + prevVar
    prevVar++
    return
  $('button[class=\'next\']').each ->
    $(this).attr 'class', 'next' + nextVar
    nextVar++
    return
  $('.carousel').each ->
    $(this).attr 'id', 'carousel' + carouselCount
    $('#carousel' + carouselCount).jCarouselLite
      btnNext: '.next' + carouselCount
      btnPrev: '.prev' + carouselCount
      visible: 1
    carouselCount++
    return
  return