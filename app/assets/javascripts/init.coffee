window.App ||= {}

App.init = ->
  carouselCount = 0
  nextVar = 0
  prevVar = 0
  $('button[class=\'prev\']').each ->
    $(this).attr 'class', 'prev' + prevVar
    prevVar++
    return
  $('button[class=\'next\']').each ->
    $(this).attr 'class', 'next' + nextVar
    nextVar++
    return
  $('.carousel').each ->
    $(this).attr 'id', 'carousel' + carouselCount
    $('#carousel' + carouselCount).jCarouselLite
      btnNext: '.next' + carouselCount
      btnPrev: '.prev' + carouselCount
      visible: 2
    carouselCount++
    return
  return

App.show = ->
  $('.carousel_show').jCarouselLite
    btnNext: '.next'
    btnPrev: '.prev'
    visible: 1
    auto: 800
    speed: 2000
  return

$(document).on "page:change", ->
  App.init()

$(document).on "click", "[data-behavior~=show-listing]", =>
  App.show()