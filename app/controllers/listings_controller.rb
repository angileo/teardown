class ListingsController < ApplicationController
  helper_method :sort_direction

  def index
    if params[:sort] == 'price'
      @listings = Listing.paginate(page:params[:page]).order("price #{sort_direction}")
    #  @inputs = Input.all.order("form_name #{sort_direction}")
    elsif params[:sort] == 'created_at'
      @listings = Listing.paginate(page:params[:page]).order("created_at #{sort_direction}")
    else
      @listings = Listing.paginate(page:params[:page])
    end
=begin
    if current_user
      @listings = current_user.listings.paginate(page:params[:page])
  else
      @listings = Listing.paginate(page:params[:page])
    end
=end
  end

  def update
    @listing = Listing.find(params[:id])

    if @listing.update_attributes(listing_params)
      redirect_to(listings_path, :notice => "The <b>#{ @listing.street_address }</b> listing has been updated successfully.")
    else
      render(:edit, :error => @ship.errors)
    end
  end

  def show
    @listing = Listing.find(params[:id])
  end

  def edit
    @listing = Listing.find(params[:id])
  end

  def new
    @listing = Listing.new
   # 2.times {@listing.listing_images.build}
   # @listing.listing_images.build
  end

  def create
    @listing = current_user.listings.build(listing_params)
   # @listing = Listing.create(listing_params)
    if @listing.save
      flash[:info]=" listing creado."
      redirect_to root_path
    else
      render 'new'
    end
  end

private
  def listing_params
    params.require(:listing).permit(:street_address, :city, :state, :zip, :price, :bedrooms, :bathrooms, :square_feet,
                                    :property_type, :description, :year_built, :lot_size, listing_images_attributes: [:_destroy, :id, :url, :description ])
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : 'asc'
  end
end
