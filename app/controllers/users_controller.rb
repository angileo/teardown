class UsersController < ApplicationController
  def new
    @user = User.new
    @types = [["Buider", 'builder', {:class=>"bold"}], ["Customer", 'customer']]
   # @ranges = [["A", 1, {:class=>"bold"}], ["B", 2], ["C", 3]] # {:class=> "bold"} is optional, use only if you need html class for option tag.
  end

  def create
    @user = User.new(user_params)
    if @user.save
      UserMailer.account_activation(@user).deliver_now
      flash[:info]="Please check your email to activate your account."
      redirect_to root_url
    else
      render 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def delete
    User.find(params[:id]).destroy
    flash[:success]="User deleted"
    redirect_to users_url
  end

  def update
    @user=User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success]="Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end

  def index
    @users = User.all
  end

  def show
    @user = User.find(params[:id])
    @listings = @user.listings
    @listing = @user.listings.build
  end

  private
  def user_params
    params.require(:user).permit(:street_address, :first_name, :last_name, :age_range, :price_range, :have_agent, :want_agent, :email,
                                    :password, :user_as, listing_images_attributes:[:url,:description,:_destroy,:id])
  end
end
