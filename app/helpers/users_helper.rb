module UsersHelper
  def admin?
    !current_user.nil? && (current_user.user_as == "Admin".downcase || current_user.user_as == "Builder".downcase)
  end
end
