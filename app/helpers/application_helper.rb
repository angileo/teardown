module ApplicationHelper
  def full_title (page_title = '')
    base_title = "RoR Tuto Sample App"
    unless page_title.empty?
      "#{page_title} | #{base_title}"
    else
      base_title
    end
  end

  def sortable(title, column, direction)
#    direction = sort_direction == "asc" ? "desc" : "asc"
    icon = %w[desc].include?(direction) ? 'up' : 'down'# sort_direction == 'desc' ? 'down' : 'up'

    link_to listings_path(:sort => "#{column}", :direction=> "#{direction}"), class: 'tiny button secondary', id: column do
      "#{title} &nbsp; <i class='fa fa-arrow-#{icon}'></i>".html_safe
    end
  end

=begin
  def link_to_remove_fields(name, f, options = {})
    f.hidden_field(:_destroy) + link_to_function(name, "remove_fields(this)", options)
  end

  def link_to_add_fields(name, f, association, options = {})
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, :child_index => "new_#{ association }") do |builder|
      render(association.to_s.singularize + "_fields", :f => builder)
    end

    link_to_function (name, "add_fields(this, "#{ association }", "#{ escape_javascript(fields) }")", options)
  end
=end
end
