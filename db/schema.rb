# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160329192721) do

  create_table "listing_images", force: :cascade do |t|
    t.string   "url"
    t.string   "description"
    t.integer  "listing_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "listing_images", ["listing_id"], name: "index_listing_images_on_listing_id"

  create_table "listings", force: :cascade do |t|
    t.string   "street_address"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.decimal  "price"
    t.integer  "bedrooms"
    t.integer  "bathrooms"
    t.integer  "square_feet"
    t.string   "property_type"
    t.text     "description"
    t.date     "year_built"
    t.integer  "lot_size"
    t.integer  "mls"
    t.string   "realtor"
    t.string   "elementary_school"
    t.string   "junior_high"
    t.string   "high_school"
    t.string   "garage"
    t.integer  "garage_number_of_spaces"
    t.integer  "master_bedroom_size"
    t.integer  "second_bedroom_size"
    t.integer  "third_bedroom_size"
    t.integer  "fourth_bedroom_size"
    t.integer  "fifth_bedroom_size"
    t.integer  "living_room_size"
    t.integer  "family_room_size"
    t.integer  "kitchen_size"
    t.integer  "dining_room_size"
    t.integer  "laundry_room_size"
    t.integer  "foyer_size"
    t.string   "number_of_fireplaces"
    t.binary   "appliances_included"
    t.text     "appliances"
    t.text     "additional_room_information"
    t.integer  "estimated_occupancy"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "user_id"
    t.string   "latitude"
    t.string   "longitude"
  end

  add_index "listings", ["user_id"], name: "index_listings_on_user_id"

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "age_range"
    t.string   "price_range"
    t.binary   "have_agent"
    t.binary   "want_agent"
    t.string   "email"
    t.string   "password"
    t.string   "user_as"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "activation_digest"
    t.boolean  "activated"
    t.datetime "activated_At"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true

end
