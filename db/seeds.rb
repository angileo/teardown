# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
require 'csv'

csv_text = File.read(Rails.root.join('lib', 'seeds', 'populate.csv'))
csv = CSV.parse(csv_text, :headers => true)
=begin
csv.each do |row|
  puts row.to_hash
end
=end
first_user = User.create!(first_name:"Admin User",
             last_name: "Admin",
             age_range: "26-34",
             price_range: "700K-899K",
             password: "123456",
             user_as: "admin",
             email: "administrator@teardown.org",
             activated: true,
=begin
             have_agent: false,
             want_agent: true,

 activated: true,
             activated_At: Time.zone.now,
             activation_digest: "sders"
=end
)
csv.each do |row|
  listing_new = first_user.listings.new(street_address: row['ADDRESS'], city: row['CITY'],
                             state: row['STATE'], zip: row['ZIP'], price: row['LIST PRICE'], square_feet: row['SQFT'],
                             bedrooms: row['BEDS'], bathrooms: row['BATHS'], property_type: row['HOME TYPE'],
                             description: 'Description about the house...', year_built: row['YEAR BUILT'], lot_size: row['LOT SIZE'], latitude: row['LATITUDE'], longitude: row['LONGITUDE'],
                             mls: row['SALE TYPE'], elementary_school: 'No information available',
                             junior_high: 'No information available', high_school: 'No information available', garage: row['PARKING TYPE'],
                             garage_number_of_spaces: row['PARKING SPOTS'], master_bedroom_size: 'No information available',second_bedroom_size: 'No information available',
                             third_bedroom_size: 'No information available',fourth_bedroom_size: 'No information available', fifth_bedroom_size: 'No information available',
                             living_room_size: 'No information available', kitchen_size: 'No information available',dining_room_size: 'No information available',
                             laundry_room_size: 'No information available', foyer_size: 'No information available', number_of_fireplaces: 'No information available',
                             appliances_included: 'No information available', appliances: 'No information available',additional_room_information: 'No information available',
                             estimated_occupancy: 'No information available',
  )
  listing_new.listing_images.build(url: 'http://mejoresnegocios.net/wp-content/uploads/2015/08/inmobiliario-9.jpg', description: 'Image description')
  listing_new.listing_images.build(url: 'http://www.construyehogar.com/wp-content/uploads/2014/02/Fachada-de-pequeña-casa-de-madera-410x272.jpg', description: 'Image description')
  listing_new.listing_images.build(url: 'http://www.defachadas.com/wp-content/uploads/2016/02/casa-moderna-con-piscina.jpg', description: 'Image description')
  listing_new.listing_images.build(url: 'http://www.defachadas.com/wp-content/uploads/2016/02/casa-moderna-con-piscina-5.jpg', description: 'Image description')
  listing_new.listing_images.build(url: 'http://www.defachadas.com/wp-content/uploads/2011/06/fotos-de-casas.jpg', description: 'Image description')
  listing_new.save
end
