class CreateListings < ActiveRecord::Migration
  def change
    create_table :listings do |t|
      t.string :street_address
      t.string :city
      t.string :state
      t.string :zip
      t.decimal :price
      t.integer :bedrooms
      t.integer :bathrooms
      t.integer :square_feet
      t.string :property_type
      t.text :description
      t.date :year_built
      t.decimal :lot_size
      t.integer :mls
      t.string :realtor
      t.string :elementary_school
      t.string :junior_high
      t.string :high_school
      t.string :garage
      t.integer :garage_number_of_spaces
      t.integer :master_bedroom_size
      t.integer :second_bedroom_size
      t.integer :third_bedroom_size
      t.integer :fourth_bedroom_size
      t.integer :fifth_bedroom_size
      t.integer :living_room_size
      t.integer :family_room_size
      t.integer :kitchen_size
      t.integer :dining_room_size
      t.integer :laundry_room_size
      t.integer :foyer_size
      t.string :number_of_fireplaces
      t.binary :appliances_included
      t.text :appliances
      t.text :additional_room_information
      t.integer :estimated_occupancy
      t.integer :lot_size

      t.timestamps null: false
    end
  end
end
