class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :age_range
      t.string :price_range
      t.binary :have_agent
      t.binary :want_agent
      t.string   :email
      t.string  :password
      t.string  :user_as

      t.timestamps null: false
    end
  end
end
